FROM adoptopenjdk/openjdk8:ubi
VOLUME /tmp
EXPOSE 9002
#COPY src /home/app/src
#COPY pom.xml /home/app
#RUN mvn -f /home/app/pom.xml clean package -DskipTests=true
#ENTRYPOINT ["java","-jar","/home/app/target/prueba-garaje-de-ideas-0.0.1-SNAPSHOT.jar"]
ARG JAR_FILE=target/prueba-garaje-de-ideas-0.0.1-SNAPSHOT.jar
ADD ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]