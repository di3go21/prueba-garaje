package prueba.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PostLoad;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;

import lombok.Data;

@Data
@Entity
public class Alumno {
	
	@Id
	@GeneratedValue
	private long id;
	
	@NotNull
	@Size(min = 3)
	private String nombre;
	
	@NotNull
	@Size(min = 2)
	private String apellido;
	
	@Column(unique = true)
	private String numeroDDocumento;
	
	@NotNull
	private LocalDate fechaDNacimiento;
	
	@CreationTimestamp
	private LocalDateTime fechaDeRegistro;
	
	@PostLoad
    protected void trimeaNombre(){
        if(nombre!=null)
        	nombre=nombre.trim();
    }
}
