package prueba.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;

import lombok.Data;

@Data
@Entity
public class Curso {

	@Id
	@GeneratedValue
	private long id;
	
	@Column(unique = true)
	@NotNull
	@Size(min = 3)
	private String nombre;
	
	@NotNull
	private LocalDateTime fechaDeInicio;
	
	@NotNull
	private LocalDateTime fechaFin;
	
	@NotNull
	@Positive
	private int maxAlumnos;
	
	
	
	@CreationTimestamp
	private LocalDateTime fechaDeRegistro;

}
