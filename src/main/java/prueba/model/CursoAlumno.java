package prueba.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
public class CursoAlumno {
	
	@Id
	@GeneratedValue
	private long id;
	
	@ManyToOne
	private Alumno alumno;
	@ManyToOne
	private Curso curso;

	private LocalDateTime fechaDeInscripcion;
	private LocalDateTime fechaDeBaja;
	

}
