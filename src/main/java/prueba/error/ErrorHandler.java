package prueba.error;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import lombok.Data;

@ControllerAdvice
public class ErrorHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> handleIllegalArgumentException(ConstraintViolationException e) {
    	ErrorBody errorBody = new ErrorBody();
    	errorBody.setStatus(400);
    	errorBody.setTitle(HttpStatus.BAD_REQUEST.name());
    	errorBody.setDetail(e.getMessage());
    	 return new ResponseEntity<>(
    			 errorBody, HttpStatus.BAD_REQUEST);
    }

}

@Data
class ErrorBody{
	
	int status;
	String title;
	String detail;
	
}
