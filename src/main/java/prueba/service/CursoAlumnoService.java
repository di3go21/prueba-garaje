package prueba.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import prueba.model.Alumno;
import prueba.model.Curso;
import prueba.model.CursoAlumno;
import prueba.repository.CursoAlumnoRepository;

@Service
public class CursoAlumnoService {

	@Autowired
	private CursoAlumnoRepository cursoAlumnoRepo;
	@Autowired
	private CursoService cursoService;

	public List<Alumno> getAlumnosPorCursoId(Long cursoId) {
		return cursoAlumnoRepo.findByCursoId(cursoId).stream().map(e -> e.getAlumno()).collect(Collectors.toList());
	}

	public CursoAlumno saveCursoAlumno(long alumnoId, long cursoId) {

		List<Alumno> alumnosPorCursoId = this.getAlumnosPorCursoId(cursoId);

		alumnosPorCursoId.forEach(al -> {
			if (al.getId() == alumnoId) {

				throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
						"El alumno de id " + alumnoId + " ya está inscrito en el curso");
			}
		});

		int maxAlumnos = cursoService.getCursoById(cursoId).getMaxAlumnos();

		int cantidadActual = alumnosPorCursoId.size();

		if (cantidadActual == maxAlumnos) {

			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "El curso está lleno");
		}

		Alumno alumno = new Alumno();
		alumno.setId(alumnoId);
		Curso curso = new Curso();
		curso.setId(cursoId);

		CursoAlumno cursoAlumno = new CursoAlumno();
		cursoAlumno.setCurso(curso);
		cursoAlumno.setAlumno(alumno);

		return this.cursoAlumnoRepo.save(cursoAlumno);
	}

	public void deleteAlumnoFromCurso(long alumnoId, long cursoId) {
		cursoAlumnoRepo.deleteByCursoIdAndAlumnoId(cursoId, alumnoId);
	}

}
