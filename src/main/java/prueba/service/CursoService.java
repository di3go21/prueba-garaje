package prueba.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import prueba.model.Curso;
import prueba.repository.CursoRepository;

@Service
public class CursoService {


	@Autowired
	private CursoRepository cursoRepo;
	
	public Curso getCursoById(Long id) {
		return cursoRepo.findById(id).get();
	}
	
	public List<Curso> getAlumnoByName(String name) {
		return cursoRepo.findByNombre(name);
	}
	
	public List<Curso> getAlumnoByFechaDeInicio(String fechaDeInicio) {
		return cursoRepo.findByNombre(fechaDeInicio);
	}
	public List<Curso> getAlumnoByFechaFin(String fechaFin) {
		return cursoRepo.findByNombre(fechaFin);
	}
	
	public Curso saveCurso(Curso curso) {
		return cursoRepo.save(curso);
	}
	
	public void deleteCurso(Long id) {
		cursoRepo.deleteById(id);
	}

	public List<Curso> findQuery(String nombre, LocalDateTime fechaInicio, LocalDateTime fechaFin) {
		return cursoRepo.findByNombreOrFechaDeInicioOrFechaFin(nombre, fechaInicio, fechaFin);
	}
	
}
