package prueba.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import prueba.model.Alumno;
import prueba.repository.AlumnoRepository;

@Service
public class AlumnoService {
	
	@Autowired
	private AlumnoRepository alumnoRepo;
	
	public Alumno getAlumnoPorId(Long id) {
		return alumnoRepo.findById(id).get();
	}

	public void deletePorId(long id) {
		alumnoRepo.deleteById(id);
	}

	public long createAlumno(Alumno alumno) {
		return alumnoRepo.save(alumno).getId();
	}

	public List<Alumno> getAllAlumnos() {
		return alumnoRepo.findAll();
	}

	public Alumno patchAlumno(Alumno alumno) {
		return alumnoRepo.save(alumno);
	}
	
	public List<Alumno> findByCriteria(String nombre, LocalDate fechaDNacimiento) {
		return alumnoRepo.findAll();
	}

}
