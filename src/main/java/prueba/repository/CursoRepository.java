package prueba.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import prueba.model.Curso;

@Repository
public interface CursoRepository extends JpaRepository<Curso, Long> {

	List<Curso> findByNombre(String nombre);

	List<Curso> findByFechaDeInicio(LocalDateTime fechaDeInicio);

	List<Curso> findByFechaFin(LocalDateTime fechaFin);

	List<Curso> findByNombreOrFechaDeInicioOrFechaFin(String nombre, LocalDateTime fechadeInicio,
			LocalDateTime fechaFin);
}
