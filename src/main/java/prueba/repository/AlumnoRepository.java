package prueba.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import prueba.model.Alumno;

@Repository
public interface AlumnoRepository extends JpaRepository<Alumno, Long> {
	
	List<Alumno> findByNombreOrFechaDNacimiento(String nombre, LocalDateTime fechaDNacimiento);

}
