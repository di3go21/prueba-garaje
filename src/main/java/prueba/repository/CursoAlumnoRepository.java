package prueba.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import prueba.model.CursoAlumno;

@Repository
public interface CursoAlumnoRepository extends JpaRepository<CursoAlumno, Long> {

	
	List<CursoAlumno> findByCursoId(long cursoId);
	
	void deleteByCursoIdAndAlumnoId(long cursoId,long alumnoId);
	
	
	
}
