package prueba.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.Data;
import prueba.model.CursoAlumno;
import prueba.service.CursoAlumnoService;

@RequestMapping("/api/cursoalumno")
@RestController
public class CursoAlumnoController {

	@Autowired
	CursoAlumnoService cursoAlumnoService;

	@PostMapping("/")
	public AltaAlumnoCursoDto alta(@RequestBody AltaAlumnoCursoDto altaAlumnoCursoDto) {
		altaAlumnoCursoDto.idsCursos.forEach(e -> cursoAlumnoService.saveCursoAlumno(altaAlumnoCursoDto.idAlumno, e));

		return altaAlumnoCursoDto;
	}

	@DeleteMapping("/")
	public void alta(@RequestBody CursoAlumno cursoAlumno) {
		cursoAlumnoService.deleteAlumnoFromCurso(cursoAlumno.getAlumno().getId(), cursoAlumno.getCurso().getId());
	}

}

@Data
class AltaAlumnoCursoDto {

	long idAlumno;
	List<Long> idsCursos = new ArrayList<>();

}
