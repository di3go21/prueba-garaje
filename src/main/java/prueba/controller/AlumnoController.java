package prueba.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import prueba.model.Alumno;
import prueba.service.AlumnoService;

@RequestMapping("/api/alumno")
@RestController
public class AlumnoController {

	@Autowired
	AlumnoService alumnoServ;

	@GetMapping("/{id}")
	public Alumno getAlumno(@PathVariable long id) {
		return alumnoServ.getAlumnoPorId(id);
	}

	@DeleteMapping("/{id}")
	public String deleteAlumno(@PathVariable long id) {
		alumnoServ.deletePorId(id);
		return "eliminado el alumno de id " + id;
	}

	@PostMapping("/")
	public long postAlumno(@RequestBody Alumno alumno) {

		if (alumno.getNombre() != null) {
			alumno.setNombre(alumno.getNombre().trim());
		}
		if (alumno.getApellido() != null) {
			alumno.setApellido(alumno.getApellido().trim());
		}
		if (alumno.getFechaDNacimiento() != null && alumno.getFechaDNacimiento().isAfter(LocalDate.now().minusYears(18))) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "El alumno no es mayor de edad");
		}

		return alumnoServ.createAlumno(alumno);
	}

	@GetMapping("/")
	public List<Alumno> getAllAlumnos(@RequestParam(required = false) String nombre,
			@RequestParam(required = false) String fechaDNacimiento) {

		if (nombre == null && fechaDNacimiento == null)
			return alumnoServ.getAllAlumnos();
		return alumnoServ.findByCriteria(nombre, LocalDate.parse(fechaDNacimiento));
	}

	@PatchMapping("/")
	public Alumno patchAlumno(@RequestBody Alumno alumno) {
		return alumnoServ.patchAlumno(alumno);
	}
}
