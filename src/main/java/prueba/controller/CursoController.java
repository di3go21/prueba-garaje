package prueba.controller;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import prueba.model.Alumno;
import prueba.model.Curso;
import prueba.service.CursoAlumnoService;
import prueba.service.CursoService;

@RequestMapping("/api/curso")
@RestController
public class CursoController {

	@Autowired
	CursoService cursoServ;
	@Autowired
	CursoAlumnoService cursoAlumnoServ;

	@GetMapping("/{id}")
	public Curso getCurso(@PathVariable long id) {
		return cursoServ.getCursoById(id);
	}

	@GetMapping("/{id}/alumnos")
	public List<Alumno> getAlumnosCurso(@PathVariable long id) {
		return cursoAlumnoServ.getAlumnosPorCursoId(id);
	}

	@DeleteMapping("/{id}")
	public String deleteCurso(@PathVariable long id) {
		if (!cursoAlumnoServ.getAlumnosPorCursoId(id).isEmpty()) {

			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No puedes borrar cursos con alumnos inscritos");
		}
		cursoServ.deleteCurso(id);
		return "eliminado el Curso de id " + id;
	}

	@PostMapping("/")
	public long postCurso(@RequestBody Curso curso) {

		curso.setNombre(curso.getNombre().trim());

		if (curso.getFechaDeInicio().isBefore(LocalDateTime.now())) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					"La fecha de inicio tiene que ser posterior a la fecha actual");
		}
		if (curso.getFechaFin().isBefore(curso.getFechaDeInicio())) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					"La fecha de fin no puede anterior a la de inicio");
		}

		return cursoServ.saveCurso(curso).getId();
	}

	@GetMapping("/")
	public List<Curso> searchCurso(@RequestParam(required = false) String nombre,
			@RequestParam(required = false) LocalDateTime fechaInicio,
			@RequestParam(required = false) LocalDateTime fechaFin) {

		return cursoServ.findQuery(nombre, fechaInicio, fechaFin);

	}

	@PatchMapping("/{id}")
	public Curso patchCurso(Curso curso) {
		return cursoServ.saveCurso(curso);
	}
}
