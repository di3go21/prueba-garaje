package prueba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaGarajeApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaGarajeApplication.class, args);
	}

}
